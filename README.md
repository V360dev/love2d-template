# Simpler Love2D Template

Not as overengineered as my other Love2D template. Carries over most of its assets, if you need them.

## Credits

* Check the `util/object.lua` file for its credits.
* Check the `util/state.lua` file for its credits.
* Check the `debug/profile.lua` file for its credits.
* Everything else by me.
