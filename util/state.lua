--[[
Copyright (c) 2010-2013 Matthias Richter

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

Except as contained in this notice, the name(s) of the above copyright holders
shall not be used in advertising or otherwise to promote the sale, use or
other dealings in this Software without prior written authorization.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
]]

local function NoOperation() end

 -- default gamestate produces error on every callback
local stateInit = setmetatable({leave = NoOperation},
		{__index = function() error("Gamestate not initialized. Use Gamestate.switch()") end})
local stack = {stateInit}
local initializedStates = setmetatable({}, {__mode = "k"})
local stateIsDirty = true

local GameState = {}

local function change_state(stack_offset, to, ...)
	local pre = stack[#stack]
	
	;(initializedStates[to] or to.init or NoOperation)(to)
	initializedStates[to] = NoOperation

	stack[#stack+stack_offset] = to
	stateIsDirty = true
	
	return (to.enter or NoOperation)(to, pre, ...)
end

function GameState.switch(to, ...)
	assert(to, "Missing argument: Gamestate to switch to")
	assert(to ~= GameState, "Can't call switch with colon operator")
	
	;(stack[#stack].leave or NoOperation)(stack[#stack])
	
	return change_state(0, to, ...)
end

function GameState.push(to, ...)
	assert(to, "Missing argument: Gamestate to switch to")
	assert(to ~= GameState, "Can't call push with colon operator")
	
	return change_state(1, to, ...)
end

function GameState.pop(...)
	assert(#stack > 1, "No more states to pop!")
	
	local pre, to = stack[#stack], stack[#stack-1]
	
	stack[#stack] = nil
	;(pre.leave or NoOperation)(pre)
	stateIsDirty = true
	
	return (to.resume or NoOperation)(to, pre, ...)
end

function GameState.current()
	return stack[#stack]
end

local allCallbacks = {}

-- fetch event callbacks from love.handlers
for k in pairs(love.handlers) do
	table.insert(allCallbacks, k)
end

function GameState.registerEvents(callbacks)
	local f
	local registry = {}
	
	callbacks = callbacks or allCallbacks
	
	for _, f in ipairs(callbacks) do
		registry[f] = love[f] or NoOperation
		love[f] = function(...)
			registry[f](...)
			return GameState[f](...)
		end
	end
end

-- forward any undefined functions
setmetatable(GameState, {__index = function(_, func)
	-- call function only if at least one 'update' was called beforehand
	if not stateIsDirty or func == "update" then
		stateIsDirty = false
		return function(...)
			return (stack[#stack][func] or NoOperation)(stack[#stack], ...)
		end
	end
	return NoOperation
end})

return GameState
