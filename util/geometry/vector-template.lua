|when first|{
local ffi = require("ffi")
local vecs = { float = {}, integer = {} }
}
ffi.cdef[[
typedef struct {
	|when i|{int}|when f|{double} |cmp_rep[, ]|{};
} v_vector|type|;
]]

local Vector|type|
local vector|type|_mt_index = {
	COMPONENTS = |cmp_num|,
	
	toString = function(self)
		return "Vector|type| (|var_name_long|) (|cmp_rep[ ", ]|{|cmp_name|: " .. self.|cmp_name| .. } ")" --"}
	end,
	
	zero = function() return Vector|type|(|cmp_rep[, ]|{0}) end,
	one = function() return Vector|type|(|cmp_rep[, ]|{1}) end,
	
	unpack = function(self) return |cmp_rep[, ]|{self.|cmp_name|} end,
	
	eq = function(self, other) return self.COMPONENTS == other.COMPONENTS and |cmp_rep[ and ]|{self.|cmp_name| == other.|cmp_name|} end,
	
	-- clone a vector|type| by just calling `Vector|type|(vec_to_clone)`. nice!
	|when i|{toFloat = function(self) return Vector|cmp_num|f(|cmp_rep[, ]|{self.|cmp_name|}) end,
	}|when f|{toInt = function(self) return Vector|cmp_num|i(|cmp_rep[, ]|{self.|cmp_name|}) end,
	
	floor = function(self) return Vector|type|(|cmp_rep[, ]|{math.floor(self.|cmp_name|)}) end,
	round = function(self) return Vector|type|(|cmp_rep[, ]|{math.floor(self.|cmp_name| + 0.5)}) end, -- todo: hmm
	ceil = function(self) return Vector|type|(|cmp_rep[, ]|{math.ceil(self.|cmp_name|)}) end,
	}
	abs = function(self) return Vector|type|(|cmp_rep[, ]|{math.abs(self.|cmp_name|)}) end,
	
	set = function(self, |cmp_rep[, ]|{})
		if type(|cmp_0_name|) ~= 'number' then
			|cmp_rep[, ]|{} = |cmp_rep[, ]|{|cmp_0_name|.|cmp_name|}
		end
		|cmp_rep|{if |cmp_name| then self.|cmp_name| = |cmp_name| end
		}return self
	end,
	
	unm = function(self) return Vector|type|(|cmp_rep[, ]|{-self.|cmp_name|}) end,
	
	adds = function(self, scalar) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| + scalar}) end,
	subs = function(self, scalar) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| - scalar}) end,
	muls = function(self, scalar) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| * scalar}) end,
	divs = function(self, scalar) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| / scalar}) end,
	mods = function(self, scalar) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| % scalar}) end,
	
	addv = function(self, other) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| + other.|cmp_name|}) end,
	subv = function(self, other) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| - other.|cmp_name|}) end,
	mulv = function(self, other) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| * other.|cmp_name|}) end,
	divv = function(self, other) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| / other.|cmp_name|}) end,
	modv = function(self, other) return Vector|type|(|cmp_rep[, ]|{self.|cmp_name| % other.|cmp_name|}) end,
	
	add = function(self, other) return type(other) == 'number' and self:adds(other) or self:addv(other) end,
	sub = function(self, other) return type(other) == 'number' and self:subs(other) or self:subv(other) end,
	mul = function(self, other) return type(other) == 'number' and self:muls(other) or self:mulv(other) end,
	div = function(self, other) return type(other) == 'number' and self:divs(other) or self:divv(other) end,
	mod = function(self, other) return type(other) == 'number' and self:mods(other) or self:modv(other) end,
	
	recip = function(self) return Vector|type|(|cmp_rep[, ]|{1 / self.|cmp_name|}) end,
	
	clamp = function(self, low, high)
		return Vector|type|(
			|cmp_rep[,
			]|{math.min(math.max(low.|cmp_name|, self.|cmp_name|), high.|cmp_name|)}
		)
	end,
	
	dot = function(self, other) return |cmp_rep[ + ]|{(self.|cmp_name| * other.|cmp_name|)} end,
	|when 2|{crossZ = function(self, other)
		return self.x * other.y - self.y * other.x
	end,}|when 3|{cross = function(self, other)
		return Vector|type|(
			self.y * other.z - self.z * other.y,
			self.z * other.x - self.x * other.z,
			self.x * other.y - self.y * other.x
		)
	end,
	}
	squaredMagnitude = function(self) return |cmp_rep[ + ]|{(self.|cmp_name| * self.|cmp_name|)} end,
	magnitude = function(self) return math.sqrt(self:squaredMagnitude()) end,
	normalize = function(self) return self:divs(self:magnitude()) end,
	
	lerp = function(self, other, progress)
		return self * (-progress + 1) + other * progress
	end,
	invLerp = function(self, other, progress)
		return (-self + progress) / (other - self)
	end,
	
	project = function(self, other)
		return (other:dot(self) / other:magnitude()) * other
	end,
	reflect = function(self, other)
		return self - (other * self:dot(other) * 2)
	end,
	|when 2|{
	angle = function(self)
		return math.atan(self.y, self.x)
	end,
	angleBetween = function(self, other)
		return math.atan(other.y - self.y, other.x - self.x)
	end,
	rotate = function(self, rad)
		local s, c = math.sin(rad), math.cos(rad)
		return Vector|type|(self.x * c - self.y * s, self.x * s + self.y * c)
	end,
	}
	getSector = function(self)
		return (
			|cmp_rep[ +
			]|{(self.|cmp_name| < 0 and bit.lshift(1, |cmp_index|) or 0)}
		)
	end,
	toSector0 = function(self)
		return self:abs()
	end,
	toSector = function(self, sector)
		return Vector|type|(
			|cmp_rep[,
			]|{self.|cmp_name| * (bit.band(bit.rshift(sector, |cmp_index|), 1) > 0 and -1 or 1)}
		)
	end,
}
local vector|type|_mt = {
	__unm = vector|type|_mt_index.unm,
	__add = vector|type|_mt_index.add,
	__sub = vector|type|_mt_index.sub,
	__mul = vector|type|_mt_index.mul,
	__div = vector|type|_mt_index.div,
	__mod = vector|type|_mt_index.mod,
	__len = vector|type|_mt_index.magnitude,
	__eq = vector|type|_mt_index.eq,
	__index = vector|type|_mt_index,
	__name = "Vector|type|",
	__tostring = vector|type|_mt_index.toString,
}
Vector|type| = ffi.metatype("v_vector|type|", vector|type|_mt)
table.insert(vecs.|var_name_long|, Vector|type|)
|when last|{
return vecs}
