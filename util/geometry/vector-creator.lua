-- This is a bit of a disaster. But it works well enough.
-- It's worse! I added unicode Symbols.
-- Run this with regular old lua to construct the vector.lua file from the vector-template.lua file.
-- It's better! I made things readable.

function readAll(file)
	local f = assert(io.open(file, "r"))
	local content = f:read("*a")
	f:close()
	return content
end

local unformattedCode = readAll("vector-template.lua")

-- Ah, here's some "documentation."
local newNewSyntax = {
	component = "the element of a vector (x, y, z)",
	variant = "the type of a vector (float, integer)",
	
	["|cmp_num|"] = "The number of total components will be put here.",
	["|var_name|"] = "The name of the variant currently being generated will be put here.",
	["|var_name_long|"] = "The long name of the variant currently being generated will be put here.",
	["|type|"] = "The number of total components AND the short variant name will be put here.",
	
	["|cmp_name|"] = "The current component letter will be put here. Only works in blocks.",
	["|cmp_`n`_name|"] = "The `n`th component letter will be put here.",
	["|cmp_index|"] = "The current component index will be put here. Only works in blocks.",
	
	["|cmp_rep|{}"] = "Repeat this block for each component. If empty block, replace with `|cmp_name|`, but that's useless.",
	["|cmp_rep[i]|{}"] = "Repeat this block for each component, inserting some text string `i` *between* each repetition. If empty block, replace with `|cmp_name|`.",
	
	["|when ~on|{}"] = [[
		Only output this block when this set condition is satisfied.
		Condition consists of:
		- '~', negates the condition if present
		- 'o', which is one of: =, <, >, <=, or >=.
		- 'n', the component to check the current component against
	]],
	["|when v|{}"] = "Only output this block when generating variant `v`.",
	
	["|when first|{}"] = "Output this block at the top of the file once.",
	["|when last|{}"] = "Output this block at the bottom of the file once.",
}

--[[ to build
cd util/geometry
~/Documents/Apps/Lua/lua54.exe vector-creator.lua
--]]

local components = {'x', 'y', 'z', 'w', 'a', 'b', 'c', 'd'}
local variants = {'f', 'i'}
local variantsLong = {"float", "integer"}
-- for i = 1, 64 do -- fun
-- 	components[i] = string.char(string.byte('a') + i - 1)
-- end
local resultingCode = ""

-- Make Vector2 to Vector4.
for component = 2, 4 do
	for variant = 1, #variants do
		local code = unformattedCode
		
		local firstTimeAround = component == 2 and variant == 1
		local lastTimeAround = component == 4 and variant == #variants
		
		-- First/Last Specializer
		code = string.gsub(code, "|when first|(%b{})", function(sub)
			return firstTimeAround and string.sub(sub, 2, #sub - 1) or ""
		end)
		code = string.gsub(code, "|when last|(%b{})", function(sub)
			return lastTimeAround and string.sub(sub, 2, #sub - 1) or ""
		end)
		
		-- Variant Specializer
		code = string.gsub(code, "|when (%l)|(%b{})", function(v, sub)
			return (variants[variant] == v) and string.sub(sub, 2, #sub - 1) or ""
		end)
		
		-- Component Specializer
		code = string.gsub(code, "|when (~?)([<>]?=?)(%d+)|(%b{})", function(negation, cmp, n, sub)
			negation = negation == "~"
			n = tonumber(n)
			local cmpResult = false
			if false then
				elseif cmp == "<=" then cmpResult = component <= n
				elseif cmp == ">=" then cmpResult = component >= n
				elseif cmp == "<" then cmpResult = component < n
				elseif cmp == ">" then cmpResult = component > n
				else cmpResult = component == n
			end
			if negation then cmpResult = not cmpResult end
			return cmpResult and string.sub(sub, 2, #sub - 1) or ""
		end)
		
		-- Delimited Repetition Block AND Normal Repetition Block
		local function replace(delimiter, expr)
			if not expr then
				expr = delimiter
				delimiter = nil
			end
			
			delimiter = delimiter and string.sub(delimiter, 2, #delimiter - 1)
			expr = expr and string.sub(expr, 2, #expr - 1)
			
			local result = ""
			for i = 1, component do
				if delimiter and #result > 0 then
					result = result .. delimiter
				end
				
				if expr and #expr > 0 then
					local replaced = expr
					replaced = string.gsub(replaced, "|cmp_name|", components[i])
					replaced = string.gsub(replaced, "|cmp_index|", i - 1) -- sorry lua users
					result = result .. replaced
				else
					result = result .. components[i]
				end
			end
			
			return result
		end
		code = string.gsub(code, "|cmp_rep(%b[])|(%b{})", replace)
		code = string.gsub(code, "|cmp_rep|(%b{})", replace)
		
		-- Constants
		code = string.gsub(code, "|cmp_num|", component)
		code = string.gsub(code, "|var_name|", variants[variant])
		code = string.gsub(code, "|var_name_long|", variantsLong[variant])
		code = string.gsub(code, "|type|", "" .. component .. variants[variant])
		code = string.gsub(code, "|cmp_(%d+)_name|", function(n)
			return components[tonumber(n) + 1] -- sorry lua users
		end)
		
		resultingCode = resultingCode .. "-- Generated file.\n" .. code
	end
end

local theResultingFile = io.open("vector.lua", "w")
theResultingFile:write(resultingCode)
theResultingFile:close()
