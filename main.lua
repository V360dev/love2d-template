-- OOP stuff.
Object = require("util.object") -- Renamed from "Classic" by rxi
OptObject = require("util.optobject")

-- Math stuff.
VectorTypes = require("util.geometry.vector")
Vec2, Vec3, Vec4 = unpack(VectorTypes.float)

-- Helpful stuff.
CallbackWrapper = require("util.callbackwrapper")
InputHandler = require "util.input" -- Renamed from "Baton" by Tesselode
PixelScreen = require("util.pixelscreen")
Util = require("util.util")

-- Gives you a console and a stats widget. Pretty good. Maybe I'm biased.
-- You can pass "false" to this and the debug tools will be disabled.
DebugHelper = require("debug.helper")(true)

function love.load()
	-- Makes everything nice and pixel-perfect.
	PixelScreen.PixelPerfect()
	
	-- Sets up the font.
	local supportedCharacters = [[ !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~◆◇▼▲▽△★☆■□☺☻←↑→↓]]
	font6x8 = love.graphics.newImageFont("resources/fonts/6x8.png", supportedCharacters)
	love.graphics.setFont(font6x8)
	
	font16 = love.graphics.newFont("resources/fonts/equipment.ttf", 16)
	love.graphics.setFont(font16)
	
	-- Makes a pixel screen that centers itself in the window.
	screen = PixelScreen(Vec2(640, 360))
	screen:centerScaleIn(Vec2(love.graphics.getDimensions()))
	CallbackWrapper:addLoveFunction("resize", function(width, height)
		screen:centerScaleIn(Vec2(width, height))
	end, "screen resize")
	
	-- Makes an input object.
	input = InputHandler.new {
		controls = {
			left  = {"key:a", "axis:leftx-", "button:dpleft"},
			right = {"key:d", "axis:leftx+", "button:dpright"},
			up    = {"key:w", "axis:lefty-", "button:dpup"},
			down  = {"key:s", "axis:lefty+", "button:dpdown"},
			a     = {"key:k", "button:a"},
		},
		pairs = {
			move = {"left", "right", "up", "down"}
		},
		joystick = love.joystick.getJoysticks()[1],
		deadzone = 0.33,
	}
	
	-- Makes a mouse object, with window and screen coordinates available.
	mouse = { window = Vec2.zero(), screen = Vec2.zero() }
	mouseGfx = love.graphics.newImage("resources/cursors/mouse.png")
	love.mouse.setVisible(false)
	CallbackWrapper:addLoveFunction("mousemoved", function(x, y)
		-- Updates untransformed mouse coords.
		mouse.window = Vec2(x, y)
		
		-- -- Updates the screen-coordinate mouse. Also shows the OS mouse if outside the screen.
		-- mouse.screen = screen:pointIn(mouse.window):floor()
		-- if Util.pointInBox(mouse.screen, Vec2.zero(), screen.size) == love.mouse.isVisible() then
		-- 	love.mouse.setVisible(not love.mouse.isVisible())
		-- end
		-- mouse.screen = mouse.screen:clamp(Vec2.zero(), screen.size)
		
		-- Or a one-liner if you don't want the mouse to change visibility.
		mouse.screen = screen:pointIn(mouse.window):clamp(Vec2.zero(), screen.size):floor()
	end, "mouse position")
	
	-- Adds a few (okay, one... so far) shortcuts to do common tasks.
	CallbackWrapper:addLoveFunction("keypressed", function(key)
		if key == "f4" then
			love.window.setFullscreen(not love.window.getFullscreen())
			screen:centerScaleIn(Vec2(love.graphics.getDimensions()))
		end
	end, "shortcuts")
	
	-- Loads the shader.
	myShader = love.graphics.newShader("resources/shaders/shader-neue.glsl")
	myShader:send("w_scr_size", { 640, 360 })
	myShader:send("dither_steps", 5)
	-- screen:setShader(myShader)
	
	-- Load stuff here.
	State = require("util.state")
	States = {
		Game = require("states.game"),
	}
	State.switch(States.Game)
	
	-- Time spent so far.
	timeSpent = 0
end

function love.update(dt)
	-- Update inputs.
	input:update()
	
	-- Update here.
	State.update(dt)
	
	-- Maybe update the debug tools.
	DebugHelper:update(dt)
	
	-- myShader:send("w_time", timeSpent)
	
	-- Push time forward.
	timeSpent = timeSpent + dt
end

function love.draw()
	-- Clear the screen.
	love.graphics.clear()
	
	love.graphics.setColor(1, 1, 1)
	screen:renderThenDraw(function()
		love.graphics.clear()
		
		-- Draw here.
		State.draw(screen)
		
		love.graphics.draw(mouseGfx, mouse.screen.x - 1, mouse.screen.y - 2)
	end)
	love.graphics.setShader()
	
	-- Maybe draw the debug tools.
	love.graphics.setColor(1, 1, 1)
	DebugHelper:draw(screen, font6x8)
end
