local Game = {}

function Game:init()
end

function Game:enter(previous)
end

function Game:resume()
end

function Game:update(dt)
end

function Game:draw()
	love.graphics.print("It works!", 0, 0)
end

function Game:leave()
end

return Game
