local State = {}

function State:init()
end

function State:enter(previous)
end

function State:resume()
end

function State:update(dt)
end

function State:draw()
end

function State:leave()
end

return State
