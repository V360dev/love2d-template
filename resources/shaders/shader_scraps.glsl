// Old subpixel effect
float ofs = 1 / w_scr_size.x / 2;
texcolor = vec4(
	heheGetFunnyEffect(tex, texture_coords - vec2(ofs, 0.0)).x,
	texcolor.y,
	heheGetFunnyEffect(tex, texture_coords + vec2(ofs, 0.0)).z,
	texcolor.w
);

// Barf mode
if (step(random(random(texture_coords.x * texture_coords.y) - w_time), 0.9) > 0.5) {
	texture_coords += vec2(
		cos(texture_coords.x + cos(texture_coords.y + w_time / 12.0) + w_time / 11.0) * .01,
		sin(texture_coords.y + sin(texture_coords.x + w_time / 11.0) + w_time / 12.0) * .01
	);
	texture_coords = vec2(
		mod(texture_coords.x, 1.0),
		mod(texture_coords.y, 1.0)
	);
}
